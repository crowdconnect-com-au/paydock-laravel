<?php

namespace CrowdConnect\Paydock;

use Illuminate\Support\ServiceProvider;

class PaydockServiceProvider extends ServiceProvider
{

    public function register()
    {
		$this->mergeConfigFrom(
			__DIR__.'/../config/paydock.php','paydock'
		);
    }

    public function boot()
    {

		
    }
}
