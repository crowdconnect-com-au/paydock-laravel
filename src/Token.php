<?php

namespace CrowdConnect\Paydock;

use CrowdConnect\Paydock\Traits\ConsumeApiTrait;

class Token {
	
	use ConsumeApiTrait;
	
	public function create($cardName, $cardNumber, $expireMonth, $expireYear, $cardCcv, $userEmail, $userPhone){
		
		$gatewayId = config('paydock.gateway_id');
		
		$body = json_encode([
			'gateway_id' => $gatewayId,
			'type' => 'card',
			'card_name' => $cardName,
			'card_number' => $cardNumber,
			'expire_month' => $expireMonth,
			'expire_year' => $expireYear,
			'card_ccv' => $cardCcv,
			'email' => $userEmail,
			'phone' => $userPhone
		]);
		
		return $this->performRequest('POST', '/v1/payment_sources/tokens',$body,true,true);

	}

}