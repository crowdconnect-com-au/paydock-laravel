<?php

namespace CrowdConnect\Paydock;

use CrowdConnect\Paydock\Traits\ConsumeApiTrait;

class Charge {
	
	use ConsumeApiTrait;
	
	public function charge($amount, $customerid, $paymentsourceid, $description, $reference ){
		
		$currency = config('paydock.currency');
		
		$body = json_encode([
			'amount' => $amount,
			'currency' => $currency,
			'customer_id' => $customerid,
			'payment_source_id' => $paymentsourceid,
			'description' => $description,
			'reference' => $reference,
		]);
		
		return $this->performRequest('POST', '/v1/charges',$body,true,false);

	}
	
}