<?php

namespace CrowdConnect\Paydock\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Response;

trait ConsumeApiTrait{
    
	public function performRequest($method, $requesturi, $body = '', $setHeaders, $setQuery){
		
		//declare variables
		$headers = [];
		$query = [];
		
		// For safetly, default to sandbox environment.
		$baseUri = config('paydock.sandbox_baseuri');
		
		// Use production environment if set in config.
		if(config('paydock.environment') == 'production'){
			$baseUri = config('paydock.production_baseuri');
		}

		// Return error if environment config was invalid.
		if ($baseUri == null){
			return ['error' => 'Invalid environment configuration'];
		}

		// Initialise GuzzleHttp Client with configured URI.
		$client = new Client(['base_uri' => $baseUri]);

		//set content-type for request body
		if ($body != '') {
			$headers['Content-Type'] = 'application/json';
		}

		// Set Paydock API secret headers
		if($setHeaders == true) {
			$headers['x-user-secret-key'] = config('paydock.secret_key');
		} 

		// Set Paydock API public key
		if($setQuery == true){
			$query['public_key'] = config('paydock.public_key');
		} 

		// Make API request
		$response = $client->request($method, $requesturi, [
			'body' => $body, 
			'headers' => $headers, 
			'query' => $query
		]);
		
		// Return the response received for the request.
		return json_decode($response->getBody()->getContents());
	}

}
