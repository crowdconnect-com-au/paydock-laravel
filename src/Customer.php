<?php

namespace CrowdConnect\Paydock;

use CrowdConnect\Paydock\Traits\ConsumeApiTrait;

class Customer {
	
	use ConsumeApiTrait;

	public function create($token, $firstname = '', $lastname = '', $email = '', $phone = ''){
		
		$body = json_encode([
			'token' => $token,
			'first_name' => $firstname,
			'last_name' => $lastname,
			'email' => $email,
			'phone' => $phone,
		]);
		
		return $this->performRequest('POST', '/v1/customers',$body,true,false);

	}
	
	public function update($customerid, $token){
		
		// $gatewayId = config('paydock.gateway_id');
		
		$body = json_encode([
			'token' => $token,
		]);
		
		return $this->performRequest('POST', '/v1/customers/'.$customerid,$body,true,false);

	}
	
		
	public function get($customerid){
		
		$body = '';
		
		return $this->performRequest('GET', '/v1/customers/'.$customerid,$body,true,false);

	}
	
	public function setDefaultPaymentSource($customerID, $paymentSourceID){
		
		$body = json_encode([
			'default_source' => $paymentSourceID,
		]);
		
		return $this->performRequest('POST', '/v1/customers/'.$customerID,$body,true,false);

	}
	
	public function deletePaymentSource($customerID, $paymentSourceID){
		
		$body = '';
	
		return $this->performRequest('DELETE', '/v1/customers/'.$customerID.'/payment_sources/'.$paymentSourceID,$body,true,false);
	
	}
	
}