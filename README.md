# A simple Laravel package for consuming the [Paydock](www.paydock.com) API by [CrowdConnect](www.crowdconnect.com.au)

This [Laravel](https://www.laravel.com) package lets you:
- Create one-time credit card tokens
- Create customers from a one-time tokens
- Retrieve customer payment sources
- Add/Remove customer payment sources and vault tokens
- Change customer default payment sources
- Charge a customer using specified or default payment source

## Requirements

guzzlehttp/guzzle

## Installation

TBD

```bash
composer require crowdconnect/paydock-laravel
```

## Configuration

Create .env variables

```
PAYDOCK_ENVIRONMENT=<Insert sandbox OR production here>
PAYDOCK_SECRET_KEY=<Insert secret key here>
PAYDOCK_PUBLIC_KEY=<Insert public key here>
PAYDOCK_GATEWAY_ID=<Insert gateway ID here>
```

## How to Use

TBD

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
